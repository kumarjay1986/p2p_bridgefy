//
//  ViewController.swift
//  P2P
//
//  Created by Jaykumar on 5/29/17.
//  Copyright © 2017 Emirates. All rights reserved.
//

import UIKit
import BFTransmitter
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var buttomView: UIView!
    @IBOutlet weak var messageText   : UITextField!
    var transmission                 : BFTransmitter!
    var myStaffId                    : String!
    var tostaffId                    : String!
    var messageTosend                      : String!
    lazy var messageList             : [ChatHistory]            = []
    var selectedStaf                 : StaffList!
    lazy var listOfChatStaff         : [StaffList]              = []
    let selfCustomeCell                                 = "selfCustomeCell"
    let staffCustomeCell                                 = "StaffCustomeCell"
    var newUserFlag                 : Bool = true
    
    @IBOutlet weak var chatListView: UITableView!
    @IBOutlet weak var keyboardConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        //Key bord show/Hide notification
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //Register Cell for chat list
        chatListView.register(UINib(nibName :"staffCustomeTableViewCell" , bundle : nil), forCellReuseIdentifier: staffCustomeCell)
        chatListView.register(UINib(nibName :"selfCustomeTableViewCell" , bundle : nil), forCellReuseIdentifier: selfCustomeCell)
        chatListView.rowHeight = UITableViewAutomaticDimension
        chatListView.estimatedRowHeight = 65
         NotificationCenter.default.addObserver(self, selector: #selector(self.reciveMessage), name:NSNotification.Name(rawValue: "ReciveMessage"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Checking if  existing chat user then featch all chat history
        if !newUserFlag{
        let manageObject = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let predicate = NSPredicate(format: "stafflist == %@", selectedStaf)
        let featchChat : NSFetchRequest<ChatHistory> = ChatHistory.fetchRequest()
        featchChat.predicate = predicate
        featchChat.returnsObjectsAsFaults = false
        do {
            let fetchedPerson1 = try manageObject.fetch(featchChat)
            for item in fetchedPerson1{
                messageList.append(item)
            }
        }catch {
            fatalError("Failed to fetch person: \(error)")
        }
            messageList.sort(by: { (first:ChatHistory , second: ChatHistory) -> Bool in
                first.senderTime! < second.senderTime!
            })
           chatListView.reloadData()
        }
        
    }
    //Generate system date and time
    private func generateSystemDate() -> String{
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy hh:mm:ss"
        let datestring = dateFormatterPrint.string(from: NSDate() as Date)
        return datestring
    
    }
    //Send Message for selected staff
    @IBAction func clickToSend(_ sender: Any) {
        if newUserFlag {
            let message = messageText.text!.components(separatedBy: "-")
            tostaffId = message[0]
            messageTosend = message.last!
        }
        else{
            let message = messageText.text!.components(separatedBy: "-")
            if message.count > 1 {
                messageTosend = message.last!
            }
            else{
                messageTosend = messageText.text!
            }
            tostaffId = selectedStaf.staffnumber
        }
        let payload : [String : Any] = ["toStaffName":tostaffId,"Message" :messageTosend,"senderInfo" : myStaffId,"senderTime" : generateSystemDate()];
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendMessage"), object: nil, userInfo: ["Message" : payload])
        print(generateSystemDate())
        messageText.text = ""
        sendMessageAddToDB(payLoad: payload)
    }
// save same message to db
    func sendMessageAddToDB(payLoad : [String :Any])
    {
      let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        if newUserFlag {
            //Checking if staff exit or not
            for item in listOfChatStaff{
                if item.staffnumber == (payLoad["toStaffName"] as! String){
                    item.unreadmessage = 0
                    let chathistory = ChatHistory(context : context)
                    chathistory.message = payLoad["Message"]! as? String
                    chathistory.toStaff = payLoad["toStaffName"]! as? String
                    chathistory.fromStaff = payLoad["senderInfo"]! as? String
                    chathistory.senderTime = payLoad["senderTime"]! as? String
                    item.addToChathistory(chathistory)
                    messageList.append(chathistory)
                    do{
                        try context.save()
                    }catch let error as NSError{
                        print("Could Not save.\(error),\(error.userInfo)")
                    }
                    self.addRowIntableView()
                    return
                }
            }
            //If staff not exist
            let stafflist = StaffList(context : context)
            stafflist.staffnumber = payLoad["toStaffName"]! as? String
            stafflist.unreadmessage = 0
            let chathistory = ChatHistory(context : context)
            chathistory.message = payLoad["Message"]! as? String
            chathistory.toStaff = payLoad["toStaffName"]! as? String
            chathistory.fromStaff = payLoad["senderInfo"]! as? String
            chathistory.senderTime = payLoad["senderTime"]! as? String
            stafflist.addToChathistory(chathistory)
             messageList.append(chathistory)
            newUserFlag = false
            listOfChatStaff.append(stafflist)
            selectedStaf = stafflist
        }
        else{
            let chathistory = ChatHistory(context : context)
            chathistory.message = payLoad["Message"]! as? String
            chathistory.toStaff = payLoad["toStaffName"]! as? String
            chathistory.fromStaff = payLoad["senderInfo"]! as? String
            chathistory.senderTime = payLoad["senderTime"]! as? String
            selectedStaf.addToChathistory(chathistory)
            messageList.append(chathistory)
        }
        do{
            try context.save()
        }catch let error as NSError{
            print("Could Not save.\(error),\(error.userInfo)")
        }
        self.addRowIntableView()
    }
    func reciveMessage(_ notification: Foundation.Notification) {
        let message = (notification as NSNotification).userInfo!["Message"] as! ChatHistory
        let staffNumber = (notification as NSNotification).userInfo!["StaffNumber"] as! StaffList
        messageList.append(message)
        if !listOfChatStaff.contains(staffNumber)
        {
            listOfChatStaff.append(staffNumber)
        }
        self.addRowIntableView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReciveMessage"), object: nil)
    }
    // adding noe row to chat list
    func addRowIntableView()  {
        chatListView.beginUpdates()
        let index = NSIndexPath(row : messageList.count-1 , section :0)
        chatListView.insertRows(at: [index as IndexPath], with: UITableViewRowAnimation.top)
        chatListView.endUpdates()
        chatListView.scrollToRow(at: index as IndexPath , at: UITableViewScrollPosition.top, animated: true)
    }
    // MARK: Keyboard management
    func keyboardShown(_ notification: Notification) {
        var keyboardInfo = notification.userInfo!
        let keyboardFrameBegin = keyboardInfo[UIKeyboardFrameBeginUserInfoKey]
        let frame: CGRect = (keyboardFrameBegin! as AnyObject).cgRectValue
        self.keyboardConstraint.constant = frame.size.height
        UIView.animate(withDuration: 0.0, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardHidden(_ notification: Notification) {
        self.keyboardConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }

}
extension ViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension ViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        let chatlist = messageList[indexPath.row] as ChatHistory
        if chatlist.fromStaff == myStaffId {
          let selfCell = tableView.dequeueReusableCell(withIdentifier:selfCustomeCell, for: indexPath) as! selfCustomeTableViewCell
            selfCell.message.text = chatlist.message
            selfCell.dateLable.text = chatlist.senderTime
            selfCell.containerView.layer.cornerRadius = 8.0
            selfCell.containerView.layer.borderWidth = 1.0
            selfCell.containerView.layer.borderColor = UIColor.lightGray.cgColor
            cell = selfCell
        }
        else{
            let staffCell = tableView.dequeueReusableCell(withIdentifier:staffCustomeCell, for: indexPath) as! staffCustomeTableViewCell
            staffCell.message.text = chatlist.message
            staffCell.staffName.text = chatlist.fromStaff
            staffCell.dateLable.text = chatlist.senderTime
            staffCell.containerView.layer.cornerRadius = 8.0
            staffCell.containerView.layer.borderWidth = 1.0
            staffCell.containerView.layer.borderColor = UIColor.lightGray.cgColor
            cell = staffCell
        }
      
        return cell!
    }

}


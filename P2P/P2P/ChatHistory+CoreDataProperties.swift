//
//  ChatHistory+CoreDataProperties.swift
//  P2P
//
//  Created by Jaykumar on 6/6/17.
//  Copyright © 2017 Emirates. All rights reserved.
//

import Foundation
import CoreData


extension ChatHistory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ChatHistory> {
        return NSFetchRequest<ChatHistory>(entityName: "ChatHistory")
    }

    @NSManaged public var fromStaff: String?
    @NSManaged public var message: String?
    @NSManaged public var senderTime: String?
    @NSManaged public var toStaff: String?
    @NSManaged public var stafflist: StaffList?

}

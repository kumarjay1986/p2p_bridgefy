//
//  StaffList+CoreDataProperties.swift
//  P2P
//
//  Created by Jaykumar on 6/6/17.
//  Copyright © 2017 Emirates. All rights reserved.
//

import Foundation
import CoreData


extension StaffList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StaffList> {
        return NSFetchRequest<StaffList>(entityName: "StaffList")
    }

    @NSManaged public var staffnumber: String?
    @NSManaged public var unreadmessage: Int16
    @NSManaged public var chathistory: NSSet?

}

// MARK: Generated accessors for chathistory
extension StaffList {

    @objc(addChathistoryObject:)
    @NSManaged public func addToChathistory(_ value: ChatHistory)

    @objc(removeChathistoryObject:)
    @NSManaged public func removeFromChathistory(_ value: ChatHistory)

    @objc(addChathistory:)
    @NSManaged public func addToChathistory(_ values: NSSet)

    @objc(removeChathistory:)
    @NSManaged public func removeFromChathistory(_ values: NSSet)

}

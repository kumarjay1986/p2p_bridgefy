//
//  staffLoginViewController.swift
//  P2P
//
//  Created by Jaykumar on 6/4/17.
//  Copyright © 2017 Emirates. All rights reserved.
//

import UIKit

class staffLoginViewController: UIViewController {

    @IBOutlet weak var staffNameText: UITextField!
    @IBOutlet weak var staffNumberText: UITextField!
    @IBOutlet weak var loginView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginView.layer.cornerRadius = 8.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Login With Staff Id
    @IBAction func loginAction(_ sender: Any) {
        if staffNumberText.text!.isEmpty == false {
            UserDefaults.standard.set(staffNumberText.text, forKey: "logInfo")
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "stafflist") as! StaffListViewController
            Vc.loggedId = staffNumberText.text
            Vc.updateFlag = false
            self.navigationController?.pushViewController(Vc, animated: true)
            
        }
        else{
            let alert = UIAlertController.init(title: "Login", message: "Please Enter Staff Number", preferredStyle: .alert)
            let action = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension staffLoginViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//
//  CustomStaffTableViewCell.swift
//  P2P
//
//  Created by Jaykumar on 5/31/17.
//  Copyright © 2017 Emirates. All rights reserved.
//

import UIKit

class CustomStaffTableViewCell: UITableViewCell {

    @IBOutlet weak var unReadMessageCount: UILabel!
    @IBOutlet weak var staffName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


//
//  StaffListViewController.swift
//  P2P
//
//  Created by Jaykumar on 5/30/17.
//  Copyright © 2017 Emirates. All rights reserved.
//

import UIKit
import CoreData
import BFTransmitter
class StaffListViewController: UIViewController {

    @IBOutlet weak var stafflist: UITableView!
    let customeCell = "CustomeCell"
    lazy var staffListArray : [StaffList] = []
    var changeValue         : StaffList!
    var loggedId            : String!
    var updateFlag          : Bool = true
    var transmission                 : BFTransmitter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Adding Right Bar Button
        let barBtn = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(goForChat))
        self.navigationItem.rightBarButtonItem = barBtn
        self.navigationItem.leftBarButtonItem = nil
        
        // Register Custome Cell for tableView
       stafflist.register(UINib(nibName :"CustomStaffTableViewCell" , bundle : nil), forCellReuseIdentifier: customeCell)
        stafflist.tableFooterView = UIView(frame: .zero)
        //Initilaze BFTTransmition
        transmission = BFTransmitter(apiKey:"92b84ac3-eb0e-4e1b-a1ed-387c165758a9")
        transmission.delegate = self
        transmission.start()
        transmission.isBackgroundModeEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Removing existing Notification with same Name
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SendMessage"), object: nil)
        //Adding New notifivation
        NotificationCenter.default.addObserver(self, selector: #selector(self.sendMessage), name:NSNotification.Name(rawValue: "SendMessage"), object: nil)
        //Featch Recode from DB Onlu List for chat staff
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let stuffFeatch : NSFetchRequest<StaffList> = StaffList.fetchRequest()
        do{
            let fetchStaff = try context.fetch(stuffFeatch)
            for item in fetchStaff{
                //When coming back from charroom will check for dublicate
                if !staffListArray.contains(fetchStaff.last!)
                {
                    staffListArray.append(item)
                }
                else{
                    //update unread Chat count
                    if changeValue != nil {
                        let index = staffListArray.index(of: changeValue)
                        changeValue.unreadmessage = 0
                        do{
                            try context.save()
                        }catch let error as NSError{
                            print("Could Not save.\(error),\(error.userInfo)")
                        }
                        staffListArray[index!] = changeValue
                        
                    }
                }

            }
        }catch {
                fatalError("Failed to fetch person: \(error)")
            }
        stafflist.reloadData()
    }
    // Notify To send Message
    func sendMessage(_ notification: Foundation.Notification) { 
        print("notification came")
        let payload = (notification as NSNotification).userInfo!["Message"] as! [String : Any]
        let option : BFSendingOption = [.broadcastReceiver,.fullTransmission]
        do{
            try transmission.send(payload , toUser: nil, options: option)
        }
        catch let err as NSError{
            print("Error :\(err.localizedDescription)")
        }

    }
    //Go to new staff chat window
    func goForChat()  {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatView") as! ViewController
        Vc.newUserFlag = true
        Vc.myStaffId = loggedId
        Vc.listOfChatStaff = staffListArray
        self.navigationController?.pushViewController(Vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //If chat come from other existing staff will update unread message count
    private func updateRow(staff : StaffList){
        stafflist.beginUpdates()
        let index = staffListArray.index(of: staff)
        let indexpath = NSIndexPath(row : index! , section :0)
        let staffCell = stafflist.cellForRow(at: indexpath as IndexPath) as! CustomStaffTableViewCell
        if staff.unreadmessage == 0 {
            staffCell.unReadMessageCount.text = ""
        }
        else{
            staffCell.unReadMessageCount.text = String(staff.unreadmessage)
        }
        stafflist.endUpdates()
    }
    //After getting message adding to DB
    func reciveMessageAddToDb(payLoad : [String :Any])  {
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        for item in staffListArray{
            // Checking if same staff Numer has all ready exit in db or not if Yes the updating chat list for that particular staff
            if item.staffnumber == (payLoad["senderInfo"] as! String){
                item.unreadmessage += 1
                updateRow(staff: item)
                let chathistory = ChatHistory(context : context)
                chathistory.message = payLoad["Message"]! as? String
                chathistory.toStaff = payLoad["toStaffName"]! as? String
                chathistory.fromStaff = payLoad["senderInfo"]! as? String
                chathistory.senderTime = payLoad["senderTime"]! as? String
                item.addToChathistory(chathistory)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReciveMessage"), object: nil, userInfo: ["Message" : chathistory , "StaffNumber" : item])
                do{
                    try context.save()
                }catch let error as NSError{
                    print("Could Not save.\(error),\(error.userInfo)")
                }
                return
            }
        }
        //Creasting New staff and chat list
        let stafflist = StaffList(context : context)
        stafflist.staffnumber = payLoad["senderInfo"]! as? String
        stafflist.unreadmessage += 1
        let chathistory = ChatHistory(context : context)
        chathistory.message = payLoad["Message"]! as? String
        chathistory.toStaff = payLoad["toStaffName"]! as? String
        chathistory.fromStaff = payLoad["senderInfo"]! as? String
        chathistory.senderTime = payLoad["senderTime"]! as? String
        stafflist.addToChathistory(chathistory)
        staffListArray.append(stafflist)
                do{
            try context.save()
        }catch let error as NSError{
            print("Could Not save.\(error),\(error.userInfo)")
        }
        self.addRowIntableView()
    }
    //After Creating New staff adding to staff list table
    func addRowIntableView()  {
        stafflist.beginUpdates()
        let index = NSIndexPath(row : staffListArray.count-1 , section :0)
        stafflist.insertRows(at: [index as IndexPath], with: UITableViewRowAnimation.top)
        stafflist.endUpdates()
        stafflist.scrollToRow(at: index as IndexPath , at: UITableViewScrollPosition.top, animated: true)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StaffListViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staffListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:customeCell, for: indexPath) as! CustomStaffTableViewCell
        let staff = staffListArray[indexPath.row]
        cell.staffName.text = staff.staffnumber
        if staff.unreadmessage == 0{
            cell.unReadMessageCount.text = ""
        }
        else
        {
            cell.unReadMessageCount.text = String(staff.unreadmessage)
        }
        return cell
    }
}
extension StaffListViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatVc = self.storyboard?.instantiateViewController(withIdentifier: "ChatView") as! ViewController
        chatVc.selectedStaf = staffListArray[indexPath.row]
        changeValue = staffListArray[indexPath.row]
        chatVc.myStaffId = loggedId
        chatVc.newUserFlag = false
        chatVc.listOfChatStaff = staffListArray
        self.navigationController?.pushViewController(chatVc, animated: true)
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let item = staffListArray[indexPath.row]
        item.unreadmessage = 0
        staffListArray[indexPath.row] = item
        do{
            try context.save()
        }catch let error as NSError{
            print("Could Not save.\(error),\(error.userInfo)")
        }
       
        
    }
}
extension StaffListViewController: BFTransmitterDelegate{
    
    //If something goes wrong during instantiation or start, the next methods will let you know the reason
    public func transmitter(_ transmitter: BFTransmitter, didFailAtStartWithError error: Error)
    {
        print(error.localizedDescription);
    }
    //if the transmitter needs internet to validate the license.
    public  func transmitter(_ transmitter: BFTransmitter, didOccur event: BFEvent, description: String)
    {
        print(description);
    }
    //When a peer is detected, the connection is made automatically. The following method is invoked when a peer has established connection:
    public func transmitter(_ transmitter: BFTransmitter, didDetectConnectionWithUser userUser: String)
    {
        print(userUser);
    }
    //When a peer is disconnected (out of range), the following method will be invoked:
    public func transmitter(_ transmitter: BFTransmitter, didDetectDisconnectionWithUser userUser: String)
    {
        print(userUser);
    }
    // If the packet was sent successfully, the following method will be invoked:
    public func transmitter(transmitter: BFTransmitter, didSendDirectPacket packetID: String)
    {
        print("packetId :\(packetID)");
    }
    // Error on sending package
    public func transmitter(transmitter: BFTransmitter, didFailForPacket packetID: String, error: NSError?)
    {
        print("packetId :\(packetID)\(String(describing: error?.localizedDescription))");
    }
    public func transmitter(_ transmitter: BFTransmitter, didReceive dictionary: [String : Any]?, with data: Data?, fromUser user: String, packetID: String, broadcast: Bool, mesh: Bool) {
        if (dictionary?["toStaffName"] as! String) == loggedId {
           self.reciveMessageAddToDb(payLoad: dictionary!)
        }
    }
    
    // a secure connection can be established.
    public func transmitter(_ transmitter: BFTransmitter, shouldConnectSecurelyWithUser user: String) -> Bool
    {
        return true
    }
    public func establishSecureConnection(user: String,error: NSError?)
    {
        print("Connection :\(user)\(String(describing: error?.localizedDescription))");
    }
    //To indicate that the secure connection was successful:
    public func transmitter(_ transmitter: BFTransmitter, didDetectSecureConnectionWithUser user: String)
    {
        
    }
    public func transmitter(_ transmitter: BFTransmitter, meshDidAddPacket packetID: String) {
        //Packet added to mesh
    }
    public func transmitter(_ transmitter: BFTransmitter, didReachDestinationForPacket packetID: String) {
        //Mesh packet reached destiny (no always invoked)
    }
    public func transmitter(_ transmitter: BFTransmitter, didSendDirectPacket packetID: String) {
        //A direct message was sent
    }
    public func transmitter(_ transmitter: BFTransmitter, didFailForPacket packetID: String, error: Error?) {
        //A direct message transmission failed.
    }
    public func transmitter(_ transmitter: BFTransmitter, meshDidDiscardPackets packetIDs: [String]) {
        //A mesh message was discared and won't still be transmitted.
    }
    public func transmitter(_ transmitter: BFTransmitter, meshDidRejectPacketBySize packetID: String) {
        print(packetID);
    }
    public func transmitter(_ transmitter: BFTransmitter, meshDidStartProcessForPacket packetID: String) {
        print(packetID);
    }
    
}

